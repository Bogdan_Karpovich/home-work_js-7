//1
function isPalindrome(str) {
   const cleanStr = str.replace(/[^a-zA-Z0-9]/g, '').toLowerCase();
   
   const reversedStr = cleanStr.split('').reverse().join('');
   
   return cleanStr === reversedStr;
}

console.log(isPalindrome("A man, a plan, a canal: Panama"));
console.log(isPalindrome("hello"));

//2
function isValidLength(str, maxLength) {
   return str.length <= maxLength;
}

console.log(isValidLength("short string", 20)); 
console.log(isValidLength("this is a very long string", 20)); 

//3
function calculateFullYears(birthdateStr) {
   const birthdate = new Date(birthdateStr);
   const today = new Date();

   let age = today.getFullYear() - birthdate.getFullYear();
   if (today.getMonth() < birthdate.getMonth() || 
      (today.getMonth() === birthdate.getMonth() && today.getDate() < birthdate.getDate())) {
       age--;
   }
   
   return age;
}

const birthdateStr = prompt("Введіть дату народження (yyyy-mm-dd):");
const fullYears = calculateFullYears(birthdateStr);

console.log(`Кількість повних років: ${fullYears}`);
